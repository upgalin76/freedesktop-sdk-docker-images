kind: autotools

depends:
- filename: bootstrap-import.bst
  junction: freedesktop-sdk.bst
- filename: public-stacks/buildsystem-autotools.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: components/libcap.bst
  junction: freedesktop-sdk.bst
- filename: components/python3.bst
  junction: freedesktop-sdk.bst
- filename: components/glib.bst
  junction: freedesktop-sdk.bst
- filename: components/pixman.bst
  junction: freedesktop-sdk.bst

config:
  install-commands:
    (>):
    - |
      for firmware in %{unused-firmware}; do
        rm "%{install-root}%{datadir}/qemu/${firmware}"
      done

variables:
  unused-firwmare: '%{other-firmware}'
  (?):
  - target_arch == "x86_64":
      targets: --target-list=x86_64-softmmu,i386-softmmu
      unused-firmware: >-
        %{other-firmware}
        %{aarch64-firmware}
        %{ppc-firmware}
  - target_arch == "aarch64":
      targets: --target-list=aarch64-softmmu,arm-softmmu
      unused-firmware: >-
        %{other-firmware}
        %{x86_64-firmware}
        %{ppc-firmware}
  - target_arch == "powerpc64le":
      targets: --target-list=ppc64-softmmu
      unused-firmware: >-
        %{other-firmware}
        %{x86_64-firmware}
        %{aarch64-firmware}

  other-firmware: >-
    hppa-firmware.img
    openbios-sparc32
    openbios-sparc64
    opensbi-riscv32-virt-fw_jump.bin
    opensbi-riscv64-sifive_u-fw_jump.bin
    opensbi-riscv64-virt-fw_jump.bin
    s390-ccw.img
    s390-netboot.img

  x86_64-firmware: >-
    edk2-i386-code.fd
    edk2-i386-secure-code.fd
    edk2-i386-vars.fd
    edk2-x86_64-code.fd
    edk2-x86_64-secure-code.fd
    firmware/50-edk2-i386-secure.json
    firmware/50-edk2-x86_64-secure.json
    firmware/60-edk2-i386.json
    firmware/60-edk2-x86_64.json
    efi-e1000.rom
    efi-e1000e.rom
    efi-eepro100.rom
    efi-ne2k_pci.rom
    efi-pcnet.rom
    efi-rtl8139.rom
    efi-vmxnet3.rom
    bios.bin
    bios-256k.bin
    vgabios-ati.bin
    vgabios-bochs-display.bin
    vgabios-cirrus.bin
    vgabios-qxl.bin
    vgabios-ramfb.bin
    vgabios-stdvga.bin
    vgabios-virtio.bin
    vgabios-vmware.bin
    vgabios.bin
    sgabios.bin
    pxe-e1000.rom
    pxe-eepro100.rom
    pxe-ne2k_pci.rom
    pxe-pcnet.rom
    pxe-rtl8139.rom
    pxe-virtio.rom

  aarch64-firmware: >-
    firmware/60-edk2-aarch64.json
    firmware/60-edk2-arm.json
    edk2-aarch64-code.fd
    edk2-arm-code.fd
    edk2-arm-vars.fd

  ppc-firmware: >-
    openbios-ppc
    ppc_rom.bin
    u-boot.e500

  unknown-firmware: >-
    kvmvapic.bin
    linuxboot.bin
    linuxboot_dma.bin
    multiboot.bin
    palcode-clipper
    petalogix-ml605.dtb
    petalogix-s3adsp1800.dtb
    pvh.bin
    qemu-nsis.bmp
    qemu_vga.ndrv
    skiboot.lid
    slof.bin
    spapr-rtas.bin
    trace-events-all
    u-boot-sam460-20100605.bin

  conf-local: >-
    --disable-werror
    --enable-system
    --enable-virtfs
    --disable-user

  conf-deterministic: ''
  conf-link-args: ''
  conf-libtool-force-dlsearch: ''
  conf-args: >-
    %{targets}
    --prefix=%{prefix}
    --bindir=%{bindir}
    --sbindir=%{sbindir}
    --sysconfdir=%{sysconfdir}
    --datadir=%{datadir}
    --includedir=%{includedir}
    --libdir=%{libdir}
    --libexecdir=%{libexecdir}
    --localstatedir=%{localstatedir}
    --sharedstatedir=%{sharedstatedir}
    --mandir=%{mandir}
    --infodir=%{infodir}
    --localstatedir=/
    %{conf-extra}
    %{conf-global}
    %{conf-local}

sources:
- kind: git_tag
  url: https://git.qemu.org/git/qemu.git
  track: master
  track-tags: True
  submodules:
    roms/seabios:
      url: https://git.qemu.org/git/seabios.git/
      checkout: True
    roms/SLOF:
      url: https://git.qemu.org/git/SLOF.git
      checkout: True
    roms/ipxe:
      url: https://git.qemu.org/git/ipxe.git
      checkout: True
    roms/openbios:
      url: https://git.qemu.org/git/openbios.git
      checkout: True
    roms/openhackware:
      url: https://git.qemu.org/git/openhackware.git
      checkout: True
    roms/qemu-palcode:
      url: https://git.qemu.org/git/qemu-palcode.git
      checkout: True
    roms/sgabios:
      url: https://git.qemu.org/git/sgabios.git
      checkout: True
    dtc:
      url: https://git.qemu.org/git/dtc.git
      checkout: True
    roms/u-boot:
      url: https://git.qemu.org/git/u-boot.git
      checkout: True
    roms/skiboot:
      url: https://git.qemu.org/git/skiboot.git
      checkout: True
    roms/QemuMacDrivers:
      url: https://git.qemu.org/git/QemuMacDrivers.git
      checkout: True
    ui/keycodemapdb:
      url: https://git.qemu.org/git/keycodemapdb.git
      checkout: True
    capstone:
      url: https://git.qemu.org/git/capstone.git
      checkout: True
    roms/seabios-hppa:
      url: https://git.qemu.org/git/seabios-hppa.git
      checkout: True
    roms/u-boot-sam460ex:
      url: https://git.qemu.org/git/u-boot-sam460ex.git
      checkout: True
    tests/fp/berkeley-testfloat-3:
      url: https://git.qemu.org/git/berkeley-testfloat-3.git
      checkout: True
    tests/fp/berkeley-softfloat-3:
      url: https://git.qemu.org/git/berkeley-softfloat-3.git
      checkout: True
    roms/edk2:
      url: https://git.qemu.org/git/edk2.git
      checkout: True
    slirp:
      url: https://git.qemu.org/git/libslirp.git
      checkout: True
